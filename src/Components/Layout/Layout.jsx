import React from "react";

import logo from '../../images/fanap-intra-logo.png';
import './Layout.scss';

const Layout = (props) => {
	return (
		<div className='layout'>
			<header className='header'>
				<div className='title'>MULTI INPUT</div>
				<a className='logo' href='https://fanap-infra.ir/' target='_blank'>
					<img src={logo} alt='fanap-intra' />
				</a>
			</header>
			<main className='main'>
				{props.children}
			</main>
			<footer className='footer'>
				This is a test project.
			</footer>
		</div>
	);
}

export default Layout;
