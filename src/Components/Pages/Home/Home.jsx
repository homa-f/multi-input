import React from "react";

import MultiInput from '../../Partials/MultiInput';
import './Home.scss';

const Home = () => {
	return (
		<div className='home'>
			<div className='container'>
				<div className='description'>Enter as many inputs as you want:</div>
				<MultiInput />
			</div>
		</div>
	);
}

export default Home;