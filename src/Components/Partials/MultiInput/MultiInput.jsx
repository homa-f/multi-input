/* global _ */

import React, { useState, useEffect } from "react";

import './MultiInput.scss';


const MultiInput = () => {

	const [allInputsFull, setAllInputsFull] = useState(false)
	const [inputData, setInputData] = useState(false)
	const [inputs, setInputs] = useState([{
		label: 'test attribute',
		name: 'test-0',
		index: 0,
		type: 'text',
		value: '',
	}])


	const onChange = index => e => {
		let key = e.target.name;
		let value = e.target.value;

		// update inputData object
		setInputData({ ...inputData, [key]: value })
		if(value == '') {
			let newInputData = _.omit(inputData, key)
			setInputData(newInputData)
		}

		// update inputs array
		let newInputs = [...inputs]
		newInputs[index].value = value;
		setInputs(newInputs)
	}


	useEffect(() => {
		if(inputs.length == 1 && inputs[0].value != '') {
			addInput()
		} else if (inputs.length > 1 && inputs[inputs.length - 1].value != '') {
			inputs.forEach(i => {
				if(i.value != '')
					setAllInputsFull(true)
			})
			if(allInputsFull)
				addInput()
		} else if (inputs.length > 1 && inputs[inputs.length - 2].value == '') {
			// remove the last input if the user clears the input before last input
			let newInputs = inputs.slice(0, -1); 
			setInputs(newInputs)
		}
	}, [inputs, allInputsFull])


	const addInput = () => {
		let lastInputIndex = inputs[inputs.length - 1].index
		setInputs([...inputs, {
			label: 'test attribute',
			name: `test-${lastInputIndex+1}`,
			index: lastInputIndex+1,
			type: 'text',
			value: '',
		}])
	}


	const removeInput = (item) => {
		// update inputs array
		let newInputs = inputs.filter(el => el.name != item.name)
		setInputs(newInputs)
		
		// update inputData object
		let newInputData = _.omit(inputData, item.name)
		setInputData(newInputData)
	}


	const handleSumbit = (e) => {
		e.preventDefault()

		// check if there is any empty input
		let thereIsEmptyInput = false
		inputs.forEach((i, index) => {
			if(i.value == '' && index != inputs.length-1)
				thereIsEmptyInput = true
		})

		if(inputData == false || Object.keys(inputData).length == 0) {
			alert('You can not submit empty form.')
		} else if(thereIsEmptyInput) {
			alert('Please fill or remove the empty inputs in between.')
		} else {
			let submitData = Object.values(inputData).filter(item => item);
			alert('The data you entered: ' + submitData)
		}
	}


	const cancel = () => {
		setInputData(false)
		setInputs([{
			label: 'test attribute',
			name: 'test-0',
			index: 0,
			type: 'text',
			value: '',
		}])
	}


	return (
		<>
		<div className='multi-input'>
			<form className='form' onSubmit={handleSumbit}>
				<div className='inputs'>
					{inputs && inputs.map((item, index) => (
						<div className='form-group' key={index}>
							<input
								type='text'
								name={item.name}
								onChange={onChange(index)}
								value={inputData[item.name] ? inputData[item.name] : ''}
								className={inputData[item.name] && 'has-value'}
								autoComplete='off'
								id={item.name}
							/>
							<label htmlFor={item.name}>{item.label}</label>
							{inputs.length > 1 &&
								<span className='remove' onClick={() => removeInput(item)}>X</span>
							}
						</div>
					))}
				</div>
				<div className='buttons'>
					<button className='btn save' type='submit'>SAVE</button>
				</div>
			</form>
			<button className='btn cancel' onClick={cancel}>CANCEL</button>
		</div>
		</>
	);
}

export default MultiInput;