import React from 'react';

import Layout from './Components/Layout';
import Home from './Components/Pages/Home';

import './main.scss';

function App() {
	return <>
		<Layout>
			<Home />
		</Layout>
	</>
}

export default App;
